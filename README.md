# Makhota

Computation of trends and convexities for Coronavirus published figures.

For this project we use the *JHU Coronavirus COVID-19 Global Cases* public database . The database can be found at [this link](https://console.cloud.google.com/marketplace/details/johnshopkins/covid19_jhu_global_cases?filter=solution-type:dataset&q=corona&id=430e16bb-bd19-42dd-bb7a-d38386a9edf5)
In order to help performing the BigQuery requests we are happly using the [BigQueryHelper project by SohierDane](https://github.com/SohierDane/BigQuery_Helper)

In summary we have the following sections

* Quick start for the jupyter notebooks
* Education


## Quick start for the jupyter notebooks

For convenience we define the environment variable MAKHOTA to point to the root of the makhota git repository. For example you can add the following line to your shell startup script, such as `~/.profile`:

```
export MAKHOTA="/root/of/the/git/repositories/makhota"
```

Currently jupyter notebooks are the main interface to use Makhota. In order to use the correct python libaries it is convenient to define a conda environment as shown below. After

```
cd "$MAKHOTA/notebooks"

source activate makhota_2020_04

export PYTHONPATH="$MAKHOTA"

jupyter notebook --port 8891
```

Once you see the notebook interface you need to run the following two notebooks:

1. `030-SAVE-most-recent-data-from-jhu-covid-19-database`
2. `070-SAVE-historical-trends-and-convexities`

in this order to download the data and saved the precomputed values. Be mindful that the second notebook might take tens of minutes. Once the data are downloaded you can use the last notebook, namely `080-EXPL-processing-of-jhu-covid-19-data`, to compute the latest charts and figures.



### First creation of the conda environment

The conda environment can be created and inistialized by following these CLI commands:


```
conda create -n makhota_2020_04 python=3.7 numpy pandas seaborn jupyter

source activate makhota_2020_04

conda install -c conda-forge notebook
conda install -c anaconda sqlalchemy
conda install -c anaconda scikit-learn
conda install -c conda-forge pybigquery
conda install -c anaconda pymysql
conda install -c conda-forge loguru
conda install -c anaconda statsmodels

pip install --upgrade google-cloud-bigquery[bqstorage,pandas]

```

## Education

If you are not familiar with BigQuery and SqlAlchemy you can read [this post](https://hackersandslackers.com/bigquery-and-sql-databases/)

The library and the notebooks are written in Python. On way to learn this language is by following
[The Python Tutorial](https://docs.python.org/3/tutorial/index.html)

To know more about the conda environment you can refer to the [Conda webpage](https://docs.conda.io/en/latest/index.html). There is also a smaller version documented in the [Miniconda webpage](https://docs.conda.io/en/latest/miniconda.html)