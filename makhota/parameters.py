import os
import os.path

world_tag = 'World'

world_exchina_tag = 'World (ex China)'

saved_data_dirname = 'saved-jhu-covid19-data'

saved_data_dir = os.path.join(os.path.dirname(__file__),
                              saved_data_dirname)

global_time_series_patt = os.path.join(saved_data_dir,
                                       'global-time-series-on-{}.csv')

hist_trend_patt = os.path.join(saved_data_dir,
                               'historical-trends-on-{}.csv')

hist_convx_patt = os.path.join(saved_data_dir,
                               'historical-convexities-on-{}.csv')


if not os.path.exists(saved_data_dir):
    os.makedirs(saved_data_dir)
