import numpy as np
import seaborn as sns

colors = sns.color_palette()

color_blue = colors[0]
color_yellow = colors[1]
color_green = colors[2]
color_red = colors[3]
color_purple = colors[4]
color_brown = colors[5]
color_pink = colors[6]
color_grey = colors[7]
color_lemon = colors[8]
color_cyan = colors[9]


class AlphaScoreStrategy:
    def __init__(self, alert_dict):
        self.tolerance = 1e-8

        self.alerts = alert_dict['alert']
        self.thresholds = alert_dict['threshold']
        self.score_ewms = alert_dict['score_ewm']
        self.scores = alert_dict['score']
        self.score_top = alert_dict['score_top']
        self.score_bottom = alert_dict['score_bottom']

        colors = sns.color_palette("deep")
        self.blue = colors[0]
        self.green = colors[1]
        self.red = colors[2]
        self.purple = colors[3]
        self.yellow = colors[4]
        self.cyan = colors[5]

    def plot(self, ax, p_code, s_code, t_min=None, t_max=None, title_str=None):
        if title_str is None:
            title_str = "{}/{}".format(p_code, s_code)

        score = self.scores[p_code]
        alert = self.alerts[p_code][s_code]
        thresh = self.thresholds[p_code][s_code]
        score_ewm = self.score_ewms[p_code][s_code]
        score_top = self.score_top[p_code]
        score_bottom = self.score_bottom[p_code]

        if t_min:
            score = score[score.index >= t_min]
            alert = alert[alert.index >= t_min]
            thresh = thresh[thresh.index >= t_min]
            score_ewm = score_ewm[score_ewm.index >= t_min]
            score_top = score_top[score_top.index >= t_min]
            score_bottom = score_bottom[score_bottom.index >= t_min]

        if t_max:
            score = score[score.index <= t_max]
            alert = alert[alert.index <= t_max]
            thresh = thresh[thresh.index <= t_max]
            score_ewm = score_ewm[score_ewm.index <= t_max]
            score_top = score_top[score_top.index <= t_max]
            score_bottom = score_bottom[score_bottom.index <= t_max]

        score    .plot(ax=ax, style='-', c=self.blue, linewidth=1, legend=False,
                       title=title_str)  # label='score',

        score_ewm.plot(ax=ax, style='-', c=self.blue,
                       linewidth=2.5, legend=False)  # label='score_ewm',

        (thresh).plot(ax=ax, style='-', c=self.red)
        (-thresh).plot(ax=ax, style='-', c=self.green)

        # Drawing the score score_ewm
        marker_size = 8
        lower_points = score_ewm[alert < -self.tolerance]
        if len(lower_points):
            lower_points.plot(ax=ax, style='o', c=self.green, ms=marker_size)

        mid_points = score_ewm[alert.apply(np.fabs) < self.tolerance]
        if len(mid_points):
            mid_points.plot(ax=ax, style='o', c=self.yellow, ms=marker_size)

        higher_points = score_ewm[alert > +self.tolerance]
        if len(higher_points):
            higher_points.plot(ax=ax, style='o', c=self.red, ms=marker_size)

        # Drawing the score points
        marker_size = 4
        lower_points = score[score < -thresh]
        if len(lower_points):
            lower_points.plot(ax=ax, style='o', c=self.green, ms=marker_size)
            lower_points.plot(ax=ax, c=self.green, ls="none", yerr=0.85)

        mid_points = score[score.apply(np.fabs) <= thresh]
        if len(mid_points):
            mid_points.plot(ax=ax, style='o', c=self.yellow, ms=marker_size)

        higher_points = score[score > thresh]
        if len(higher_points):
            higher_points.plot(ax=ax, style='o', c=self.red, ms=marker_size)
            higher_points.plot(ax=ax, c=self.red, ls="none", yerr=0.85)

        # Plot top and bottom quantile
        score_top.plot(ax=ax, c=self.red, linewidth=1)
        score_bottom.plot(ax=ax, c=self.green, linewidth=1)
