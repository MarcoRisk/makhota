import glob
import logging
import pathlib

import pandas as pd

from makhota import parameters


def set_logger_to_debug():
    logger = logging.getLogger()
    logger.setLevel(logging.DEBUG)
    logging.info("enabled debug in logger for danilib on {}"
                 .format(pathlib.Path(__file__).parent.parent))
    return logger


def set_logger_to_info():
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)
    logging.info("enabled info in logger for danilib on {}"
                 .format(pathlib.Path(__file__).parent.parent))
    return logger


class NoFileMatchinException(Exception):
    pass


class HistoryLoader:
    def __init__(self, file_patt):
        self.file_patt = file_patt

    def __call__(self):
        sorted_hist_paths = sorted(glob.glob(self.file_patt.format('*')))

        if len(sorted_hist_paths) > 0:
            latest_hist_path = sorted_hist_paths[-1]
        else:
            raise NoFileMatchinException(
                f"No file matching {self.file_patt.format('*')}")

        df = pd.read_csv(latest_hist_path, header=[0, 1], index_col=0,
                         parse_dates=[0]).asfreq("D").dropna(how='all')

        return df


load_latest_global_time_series = HistoryLoader(
    parameters.global_time_series_patt)

load_latest_hist_trend = HistoryLoader(parameters.hist_trend_patt)

load_latest_hist_convx = HistoryLoader(parameters.hist_convx_patt)
