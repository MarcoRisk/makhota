"""
Pre-processing BigQuery data from the JHU COVID-19 database
"""

import os
import os.path

import numpy as np
import pandas as pd

# Constants
base_columns = ['province_state', 'country_region',
                'latitude', 'longitude', 'location_geom']
data_column_index = 5


def place_tag(series):
    if 'province_state' not in series or series['province_state'] is None or series['province_state'] is np.nan:
        if 'country_region' in series:
            return series['country_region']
        else:
            return series.name
    elif 'country_region' in series:
        return f"{series['country_region']}({series['province_state']})"
    else:
        return series.name


def preprocess(raw_data):
    base_df = raw_data[base_columns]

    # Transform column data in rows
    data_column_names = raw_data.columns[data_column_index:]
    time_series_df = raw_data[data_column_names].stack().unstack(
        'country-index')
    time_series_df.index.name = 'date'

    # Parse dates from column names
    date_list = []
    for col_tag in time_series_df.index:
        _, month, day, year2 = col_tag.split("_")
        date_ts = pd.Timestamp(
            day=int(day),  month=int(month), year=int('20'+year2))
        date_list.append(date_ts)

    time_series_df.index = date_list
    time_series_df.index.name = 'date'

    # Using country tags as column names
    places = [place_tag(base_df.iloc[i]) for i in range(len(base_df))]

    time_series_df.columns = places
    time_series_df.columns.name = 'place'

    return time_series_df
