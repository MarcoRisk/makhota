import collections
import logging
# import math

from matplotlib import pyplot

import numpy as np
import pandas as pd
import statsmodels.api as sm

from makhota import plotters

# Forecast parameters
log_threshold = 10
lookahead_steps = 5

# Plot parameters
deriv_label_format = "{:+.3%}"
vol_label_format = "({:.1%})"
y_label_position_pct = 1.0025
x_label_position_pct = 0.333
vol_label_inf = 99.991
vol_annualization = 250


class LocalLinearTrend(sm.tsa.statespace.MLEModel):
    """
    Univariate Local Linear Trend Model
    """

    def __init__(self, endog):
        # Model order
        k_states = k_posdef = 2

        # Initialize the statespace
        super(LocalLinearTrend, self).__init__(
            endog, k_states=k_states, k_posdef=k_posdef,
            initialization='approximate_diffuse',
            loglikelihood_burn=k_states,
            # added by MM
            # freq='D'
        )

        # Initialize the matrices
        self.ssm['design'] = np.array([1, 0])
        self.ssm['transition'] = np.array([[1, 1],
                                           [0, 1]])
        self.ssm['selection'] = np.eye(k_states)

        # Cache some indices
        self._state_cov_idx = ('state_cov',) + np.diag_indices(k_posdef)

    @property
    def param_names(self):
        return ['sigma2.measurement', 'sigma2.level', 'sigma2.trend']

    @property
    def start_params(self):
        return [np.std(self.endog)]*3

    def transform_params(self, unconstrained):
        return unconstrained**2

    def untransform_params(self, constrained):
        return constrained**0.5

    def update(self, params, *args, **kwargs):
        params = super(LocalLinearTrend, self).update(params, *args, **kwargs)

        # Observation covariance
        self.ssm['obs_cov', 0, 0] = params[0]

        # State covariance
        self.ssm[self._state_cov_idx] = params[1:]


class LLT1D:
    std_tolerance = 1e-10

    def __init__(self, time_series):
        if time_series.std() < self.std_tolerance:
            raise Exception(
                f"Time series Std-Dev is too low ({time_series.std()})")

        self.time_series = time_series.copy()
        self.time_series.name = 'actual'
        self.time_series.index.freq = pd.infer_freq(self.time_series.index)

        self.log_time_series = time_series[time_series
                                           >= log_threshold].apply(np.log)

        # Compute observed volatilty
        self.observed_vol = self.log_time_series.std() * np.sqrt(vol_annualization)

        # Setup the model
        self.mod = LocalLinearTrend(self.log_time_series)

        # Fit it using MLE (recall that we are fitting the three variance parameters)
        self.res = self.mod.fit(disp=False)

    def summary(self):
        return self.res.summary()

    def get_forecast(self, lookahead_steps, confidence=0.25):

        # Predict
        log_predict = self.res.get_prediction()

        log_predict_ci = log_predict.conf_int(alpha=confidence)
        log_predict_ci.columns = ['low', 'high']
        log_predict_ci['mid'] = log_predict.predicted_mean

        predict_ci_tmp = log_predict_ci.iloc[2:].apply(np.exp)
        predict_ci_tmp['trend'] = predict_ci_tmp['mid'].pct_change()

        self.predict_ci = predict_ci_tmp.join(self.time_series)

        # Predict
        log_forecast = self.res.get_forecast(lookahead_steps)
        log_forecast_ci = log_forecast.conf_int(alpha=confidence)
        log_forecast_ci.columns = ['low', 'high']
        log_forecast_ci['mid'] = log_forecast.predicted_mean

        self.forecast_ci = log_forecast_ci.apply(np.exp)
        self.forecast_ci['trend'] = self.forecast_ci['mid'].pct_change()

        trend_cont = self.forecast_ci.iloc[-1]['trend']
        # Compute the daily-compounded trend
        trend_daily = np.exp(trend_cont)-1.0

        self.trend = trend_daily

        return self.forecast_ci

    def get_trend_history(self, confidence, skip_days=0, from_date=None):
        logger = logging.getLogger(__name__+".get_trend_history")

        lookahead_steps = 5
        self.get_forecast(lookahead_steps, confidence)
        predict_ci = self.predict_ci.iloc[skip_days:]

        if from_date:
            date_range = predict_ci.loc[from_date:].index
        else:
            date_range = predict_ci.index

        logger.debug(f'date_range is {date_range}')

        trend_dict = {}
        for date in date_range:
            llt1d = LLT1D(self.time_series[self.time_series.index <= date])
            llt1d.get_forecast(lookahead_steps, confidence)
            trend_dict[date] = llt1d.trend

        trend_s = pd.Series(trend_dict)

        return trend_s


def one_step_right_slope(trend):
    slope = trend - trend.shift()
    return slope


def two_steps_right_slope(trend):
    trend_shift = trend.shift()
    slope = trend*2.0/3.0 - trend_shift/3.0 - trend_shift.shift()/3.0
    return slope


def three_steps_right_slope(trend):
    slope = trend/2.0 - trend.shift()/6.0 \
        - trend.shift().shift()/6.0 - trend.shift().shift().shift()/6.0

    return slope
